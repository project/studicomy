<?php
$vars = get_defined_vars();
$view = studicomy_art_get_drupal_view();
$view->print_maintenance_head($vars);
if (isset($page))
foreach (array_keys($page) as $name)
$$name = & $page[$name];
$art_sidebar_left = isset($sidebar_left) && !empty($sidebar_left) ? $sidebar_left : NULL;
$art_sidebar_right = isset($sidebar_right) && !empty($sidebar_right) ? $sidebar_right : NULL;
if (!isset($vnavigation_left)) $vnavigation_left = NULL;
if (!isset($vnavigation_right)) $vnavigation_right = NULL;
$tabs = (isset($tabs) && !(empty($tabs))) ? '<ul class="arttabs_primary">'.render($tabs).'</ul>' : NULL;
$tabs2 = (isset($tabs2) && !(empty($tabs2))) ?'<ul class="arttabs_secondary">'.render($tabs2).'</ul>' : NULL;
$is_maintenance = (bool)strpos($template_file, 'maintenance-page.tpl.php');
?>

<div id="stud-main">
<header class="stud-header"><?php if (!empty($art_header)) { echo render($art_header); } ?>

    <div class="stud-shapes">
        
            </div>
<div class="stud-slider stud-slidecontainerheader" data-width="1920" data-height="225">
    <div class="stud-slider-inner">
<div class="stud-slide-item stud-slideheader0">

<div class="stud-textblock stud-slideheader0-object1275252263">
        <div class="stud-slideheader0-object1275252263-text-container">
        <div class="stud-slideheader0-object1275252263-text"><p style="color: #E2EEF9; font-size:20px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:bold;font-style:normal;text-decoration:none;text-transform:uppercase">Global Solutions</p><p style="color: #CDE2F4; font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none">Easy to Remember</p><p style="font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none"><a style="color: #E2EEF9" href="#">More...</a></p></div>
    </div>
    
</div>
</div>
<div class="stud-slide-item stud-slideheader1">

<div class="stud-textblock stud-slideheader1-object972822620">
        <div class="stud-slideheader1-object972822620-text-container">
        <div class="stud-slideheader1-object972822620-text"><p style="color: #FCEFDE; font-size:20px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:bold;font-style:normal;text-decoration:none;text-transform:uppercase">Let's Create</p><p style="color: #FAE4C6; font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none">Hello and Welcome.</p><p style="font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none"><a style="color: #FCEFDE" href="#">More...</a></p></div>
    </div>
    
</div>
</div>
<div class="stud-slide-item stud-slideheader2">

<div class="stud-textblock stud-slideheader2-object2044370354">
        <div class="stud-slideheader2-object2044370354-text-container">
        <div class="stud-slideheader2-object2044370354-text"><p style="color: #EDEDED; font-size:20px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:bold;font-style:normal;text-decoration:none;text-transform:uppercase">Vision. Mission. Strategy</p><p style="color: #E0E0E0; font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none">Hello and Welcome.</p><p style="font-size:11px;font-family:'Trebuchet MS', Arial, Helvetica, Sans-Serif;font-weight:normal;font-style:normal;text-decoration:none"><a style="color: #EDEDED" href="#">More →</a></p></div>
    </div>
    
</div>
</div>

    </div>
</div>
<div class="stud-slidenavigator stud-slidenavigatorheader" data-left="1" data-top="1">
<a href="#" class="stud-slidenavigatoritem"></a><a href="#" class="stud-slidenavigatoritem"></a><a href="#" class="stud-slidenavigatoritem"></a>
</div>


<?php if (!empty($site_name)) : ?>
<?php if (!$title) : ?>
<h1 class="stud-headline"><a href="<?php echo check_url($front_page); ?>" title = "<?php echo $site_name; ?>"><?php echo $site_name;  ?></a></h1><?php else : ?><div class="stud-headline"><a href="<?php echo check_url($front_page); ?>" title = "<?php echo $site_name; ?>"><?php echo $site_name;  ?></a></div><?php endif; ?><?php endif; ?>





<?php if (!empty($navigation) || !empty($extra1) || !empty($extra2)): ?>
<nav class="stud-nav">
     
    <?php if (!empty($extra1)) : ?>
<div class="stud-hmenu-extra1"><?php echo render($extra1); ?></div>
<?php endif; ?>
<?php if (!empty($extra2)) : ?>
<div class="stud-hmenu-extra2"><?php echo render($extra2); ?></div>
<?php endif; ?>
<?php if (!empty($navigation)) : ?>
<?php echo render($navigation); ?>
<?php endif; ?>
</nav><?php endif; ?>


                    
</header>
<div class="stud-sheet clearfix">
            <?php if (!empty($banner1)) { echo '<div id="banner1">'.render($banner1).'</div>'; } ?>
<?php echo studicomy_art_placeholders_output(render($top1), render($top2), render($top3), 'tops'); ?>
<div class="stud-layout-wrapper">
                <div class="stud-content-layout">
                    <div class="stud-content-layout-row">
                        <div class="stud-layout-cell stud-content"><?php if (!empty($banner2)) { echo '<div id="banner2">'.render($banner2).'</div>'; } ?>
<?php if ((!empty($user1)) && (!empty($user2))) : ?>
<div id="user-1-2" class="stud-content-layout">
    <div class="stud-content-layout-row">
        <div class="stud-layout-cell half-width"><?php echo render($user1); ?></div>
        <div class="stud-layout-cell"><?php echo render($user2); ?></div>
    </div>
</div>
<?php else: ?>
<?php if (!empty($user1)) { echo '<div id="user1">'.render($user1).'</div>'; }?>
<?php if (!empty($user2)) { echo '<div id="user2">'.render($user2).'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner3)) { echo '<div id="banner3">'.render($banner3).'</div>'; } ?>

<?php if (!empty($breadcrumb)): ?>
    <div class="breadcrumb-parent">
<article class="stud-post stud-article">
                                
                                <div class="stud-postcontent"><?php { echo $breadcrumb; } ?>
</div>
                                
                

</article>    </div>
<?php endif; ?>
<?php $art_post_position = strpos($content, "stud-postcontent"); ?>
<?php if (($is_front || (isset($node) && isset($node->nid))) && !$is_maintenance): ?>

<?php if (!empty($tabs) || !empty($tabs2)): ?>
<article class="stud-post stud-article">
                                
                                <div class="stud-postcontent"><?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
</div>
                                
                

</article><?php endif; ?>

<?php if (!empty($mission) || !empty($help) || !empty($messages) || !empty($action_links)): ?>
<article class="stud-post stud-article">
                                
                                <div class="stud-postcontent"><?php if (isset($mission) && !empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
<?php if (!empty($help)) { echo render($help); } ?>
<?php if (!empty($messages)) { echo $messages; } ?>
<?php if (isset($action_links) && !empty($action_links)): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
</div>
                                
                

</article><?php endif; ?>

<?php if ($art_post_position === FALSE): ?>
<article class="stud-post stud-article">
                                
                                <div class="stud-postcontent"><?php endif; ?>
<?php echo studicomy_art_content_replace($content); ?>
<?php if ($art_post_position === FALSE): ?>
</div>
                                
                

</article><?php endif; ?>

<?php else: ?>

<?php $isEmpty = empty($title) && empty($tabs) && empty($tabs2) && empty($mission) && empty($help) && empty($messages) && empty($action_links); ?>
<?php
$head = $isEmpty ? '' : <<< EOT
<article class="stud-post stud-article">
	<div class="stud-postcontent">
EOT;
$tail = $isEmpty ? '' : <<< EOT
	</div>
</article>
EOT;
$content = studicomy_art_content_replace($content);
$newContent = $art_post_position ? <<< EOT
	$tail
	$content
EOT
: <<< EOT
	$content	
	$tail
EOT;
?>

<?php echo $head; ?>
<?php print render($title_prefix); ?>
<?php if (!empty($title)): print '<h1'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h1>'; endif; ?>
<?php print render($title_suffix); ?>
<?php if (!empty($tabs)) { echo $tabs.'<div class="cleared"></div>'; }; ?>
<?php if (!empty($tabs2)) { echo $tabs2.'<div class="cleared"></div>'; } ?>
<?php if (isset($mission) && !empty($mission)) { echo '<div id="mission">'.$mission.'</div>'; }; ?>
<?php if (!empty($help)) { echo render($help); } ?>
<?php if (!empty($messages)) { echo $messages; } ?>
<?php if (isset($action_links) && !empty($action_links)): ?>
<ul class="action-links">
  <?php print render($action_links); ?>
</ul>
<?php endif; ?>
<?php echo $newContent; ?>

<?php endif; ?>

<?php if (!empty($banner4)) { echo '<div id="banner4">'.render($banner4).'</div>'; } ?>
<?php if ((!empty($user3)) && (!empty($user4))) : ?>
<div id="user-3-4" class="stud-content-layout">
    <div class="stud-content-layout-row">
        <div class="stud-layout-cell half-width"><?php echo render($user3); ?></div>
        <div class="stud-layout-cell"><?php echo render($user4); ?></div>
    </div>
</div>
<?php else: ?>
<?php if (!empty($user3)) { echo '<div id="user3">'.render($user3).'</div>'; }?>
<?php if (!empty($user4)) { echo '<div id="user4">'.render($user4).'</div>'; }?>
<?php endif; ?>
<?php if (!empty($banner5)) { echo '<div id="banner5">'.render($banner5).'</div>'; } ?>
</div>
                        <?php if (!empty($art_sidebar_left) || !empty($vnavigation_left)) : ?>
<div class="stud-layout-cell stud-sidebar1"><?php echo render($vnavigation_left); ?>
<?php echo render($art_sidebar_left); ?>
</div><?php endif; ?>
                    </div>
                </div>
            </div><?php echo studicomy_art_placeholders_output(render($bottom1), render($bottom2), render($bottom3), 'bottoms'); ?>
<?php if (!empty($banner6)) { echo '<div id="banner6">'.render($banner6).'</div>'; } ?>
<footer class="stud-footer"><?php
$footer = render($footer_message);
if (isset($footer) && !empty($footer) && (trim($footer) != '')) { echo $footer; } // From Drupal structure
elseif (!empty($art_footer) && (trim($art_footer) != '')) { echo $art_footer; } // From CMSDRUPAL Content module
else { // HTML from CMSDRUPAL preview
ob_start(); ?>

<div class="stud-content-layout">
    <div class="stud-content-layout-row">
    <div class="stud-layout-cell layout-item-0" style="width: 25%">
        <p style="font: 18px 'Trebuchet MS'; color: #FFFFFF">INFO</p>
        <br />
        <ul>
        <li><a href="#">Welcome</a></li>
        <li><a href="#">People</a></li>
        <li><a href="#">Management</a></li>
        </ul>
    </div><div class="stud-layout-cell layout-item-0" style="width: 25%">
        <p style="font: 18px 'Trebuchet MS'; color: #FFFFFF">LOCATION</p>
        <br />
        <ul>
        <li><a href="#">Map</a></li>
        <li><a href="#">Address</a></li>
        <li><a href="#">Contact Us</a></li>
        </ul>
    </div><div class="stud-layout-cell layout-item-0" style="width: 25%">
        <p style="font: 18px 'Trebuchet MS'; color: #FFFFFF">ABOUT</p>
        <br />
        <ul>
        <li><a href="#">Company</a></li>
        <li><a href="#">Terms</a></li>
        </ul>
    </div><div class="stud-layout-cell layout-item-0" style="width: 25%">
        <p style="text-align: right;"><a href="#"><img width="32" height="32" alt="" src="images/rss_32-2.png" class="" /></a><a href="#"><img width="32" height="32" alt="" src="images/twitter_32.png" class="" /></a><a href="#"><img width="32" height="32" alt="" src="images/facebook_32-2.png" /></a></p>
    </div>
    </div>
</div>
<div class="stud-content-layout">
    <div class="stud-content-layout-row">
    <div class="stud-layout-cell layout-item-0" style="width: 100%">
        <p style="text-align: center;">Copyright © 2011-2012. All Rights Reserved.</p>
    </div>
    </div>
</div>

<?php
  $footer = str_replace('%YEAR%', date('Y'), ob_get_clean());
  echo studicomy_art_replace_image_path($footer);
}
?>
<?php if (!empty($copyright)) { echo '<div id="copyright">'.render($copyright).'</div>'; } ?>
</footer>

    </div>
    <p class="stud-page-footer">
        <span id="stud-footnote-links">Designed by <a target="_blank" href="http://drupalforum.byethost10.com/">drupalforumbye</a>.</span>
    </p>
</div>

<?php $view->print_closure($vars); ?>
