<div class="<?php if (isset($classes)) print $classes; ?>" id="<?php print $block_html_id; ?>"<?php print $attributes; ?>>
<div class="stud-box stud-post">
<div class="stud-box-body stud-post-body">
<article class="stud-post-inner stud-article">
<?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
<h2 class="stud-postheader"><?php print $block->subject ?></h2>
<?php endif;?>
<?php print render($title_suffix); ?>
<div class="stud-postcontent">
<div class="stud-article content">
<?php print $content; ?>
</div>
</div>
<div class="cleared"></div>
</article>
<div class="cleared"></div>
</div>
</div>
</div>
