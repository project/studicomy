<div id="<?php print $block->region .'-'. $block->module .'-'. $block->delta; ?>">
<div class="stud-box stud-post">
<div class="stud-box-body stud-post-body">
<article class="stud-post-inner stud-article">
<?php if ($block->subject): ?>
<h2 class="stud-postheader"><?php print $block->subject ?></h2>
<?php endif;?>
<div class="stud-postcontent">
<div class="stud-article content">
<?php print $block->content ?>
</div>
</div>
<div class="cleared"></div>
</article>
<div class="cleared"></div>
</div>
</div>
</div>
