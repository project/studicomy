<?php 
$vars = get_defined_vars();
$view = studicomy_art_get_drupal_view();
$message = $view->get_incorrect_version_message();
if (!empty($message)) {
print $message;
die();
}
?>
<div id="node-<?php print $node->nid; ?>" class="node<?php if(!empty($type)) { echo ' '.$type; } if ($sticky) { echo ' sticky'; } if ($promote) { print ' promote'; } if (!$status) { print ' node-unpublished'; } ?>">
<article class="stud-post stud-article">
                                <div class="stud-postmetadataheader">
                                        <?php if (!empty($title)): ?>
<?php echo studicomy_art_node_title_output($title, $node_url, $page); ?>
<?php endif; ?>

                                                            <?php if ($submitted): ?>
<div class="stud-postheadericons stud-metadata-icons"><?php echo studicomy_art_submitted_worker($date, $name); ?>
</div><?php endif; ?>

                                    </div>
                                <div class="stud-postcontent stud-postcontent-0 clearfix"><div class="stud-article">
  <?php print $picture; ?>
  <?php echo $content; ?>
  <?php if (isset($node->links['node_read_more'])) { echo '<div class="read_more">'.studicomy_art_html_link_output($node->links ['node_read_more']).'</div>'; }?>
</div>
</div>
                                <?php if (studicomy_art_links_set($node->links) || !empty($terms)):
$output = studicomy_art_node_worker($node);
if (!empty($output)):?>
<div class="stud-postmetadatafooter">
                                        <div class="stud-postfootericons stud-metadata-icons"><?php echo $output; ?>
</div>
                                    </div><?php endif; endif; ?>

                

</article></div>
